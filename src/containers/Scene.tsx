import React, { useState } from 'react';
import styled from '@emotion/styled';
import { Button, Form, Container, Row, Col, Image, Spinner } from 'react-bootstrap';
// import { useQuery } from '@apollo/client';
import { useLazyQuery } from "@apollo/client";
import { GET_SCENE_BY_ID } from '../operations/queries/MockupScene';
import { useUpdateMockupScene } from '../operations/mutations/UpdateMockupScene';
import * as GetMockupSceneTypes from '../operations/queries/__generated__/GetMockupScene';
import { currentSceneIdVar } from '../cache';
import { MockupTagInput, UpdateMockupTagInput } from '../__generated__/globalTypes';
import medias from '../media.json';

// useLazyQuery
//https://www.apollographql.com/blog/apollo-client-react-how-to-query-on-click-c1d4fecf9b66/

const Editor = (): JSX.Element => {

  const Scene = styled.div`
    position: block;
    margin-left: auto;
    margin-right: auto;
    padding-top: 30px;
  `;

  // const {
  //   data,
  //   loading,
  //   error,
  // } = useQuery<
  // GetMockupSceneTypes.GetMockupScene,
  // GetMockupSceneTypes.GetMockupSceneVariables
  // >(GET_SCENE_BY_ID, {variables: {sceneId: ''}});
  // if (error && !data) return <p>ERROR</p>;
  const [inputValue, setInputValue] = useState<string>('');
  const [isUpdate, setIsUpdate] = useState<Boolean>(false);

  const currentSceneId = currentSceneIdVar();

  const [
    getScene,
    { loading, data }
  ] = useLazyQuery<
    GetMockupSceneTypes.GetMockupScene,
    GetMockupSceneTypes.GetMockupSceneVariables
  >(GET_SCENE_BY_ID, { variables: { sceneId: currentSceneId } });

  const mockupScene = data?.mockupScene;
  // // 直接測試mockup scene
  // const { mutate } = useUpdateMockupScene();
  // if (mockupScene && !isUpdate) {
  //   const addList: MockupTagInput[] = [];
  //   medias.forEach((media) => {
  //     var modal = media as MockupTagInput;
  //     modal.position = {
  //       x: 0.0,
  //       y: 0.0,
  //       z: 0.0
  //     };
  //     modal.rotation = {
  //       x: 0.0,
  //       y: 0.0,
  //       z: 0.0,
  //       w: 0.0
  //     };
  //     modal.scale = {
  //       x: 0.0,
  //       y: 0.0,
  //       z: 0.0
  //     }
  //     addList.push(modal);
  //   });
  //   const input: UpdateMockupTagInput = {
  //     add: addList,
  //     del: [],
  //   }
  //   mutate({
  //     variables: {
  //       id: mockupScene.id,
  //       input: input
  //     }
  //   });
  //   setIsUpdate(true);
  // } 

  return (
    <Scene>
      <Container>
        <Row>
          <Col sm={4}>
            <Form.Group>
              <Form.Label>SceneId</Form.Label>
              <Form.Control onChange={e => setInputValue(e.target.value)} value={currentSceneId} />
            </Form.Group>
            <Button variant="primary" onClick={() => getScene()}>Fetch</Button>{' '}
            {loading ? <Spinner style={{ marginLeft: 30, marginTop: 10 }} animation="border" variant="secondary" /> : null}
          </Col>
          <Col sm={8}>
            <Image src={mockupScene?.thumbnail ?? 'https://cdn-images-dev.istaging.com/icons/placeholder-image.png'} thumbnail />
          </Col>
        </Row>
      </Container>
    </Scene>
  );
}

export default Editor;