import React, { useState } from 'react';
import './App.css';
import { Jumbotron, Tabs, Tab,  } from 'react-bootstrap';
import Editor from './Editor';
import Scene from './Scene';
import Home from './Home';

function App() {

  const [key, setKey] = useState<String>('home');

  return (
    <div className="App">
      <Jumbotron>
        <h1 className="header" style={{textAlign: "center"}}>IT後台管理介面</h1>
      </Jumbotron>
      <Tabs
        id="controlled-tab"
        activeKey={key}
        onSelect={(k) => setKey(k || 'home')}>
        <Tab eventKey="home" title="所有3D空間">
          <Home />
        </Tab>
        <Tab eventKey="scene" title="目前專案">
          <Scene />
        </Tab>
        <Tab eventKey="editor" title="編輯專案（POC）">
          <Editor />
        </Tab>
      </Tabs>
    </div>
  );
}

export default App;
