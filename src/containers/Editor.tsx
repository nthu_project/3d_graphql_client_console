import React from 'react';
import MediaTags from '../components/MediaTags';
import { Container, Row, Col, Button } from 'react-bootstrap';

const Editor = () => {

  return (
    <Container>
      <Row>
        <Button variant="primary" style={{ margin: 15 }}>CreateLocalTag</Button>{' '}
        <Button variant="primary" style={{ margin: 15, backgroundColor: 'black', borderColor: 'black', }}>SaveIntoServer</Button>{' '}
      </Row>
      <Row>
        <Col>
          <MediaTags label="Local" />
        </Col>
        <Col>
          <MediaTags label="Remote" />
        </Col>
      </Row>
    </Container>
  );
}

export default Editor;