import React, { useState } from 'react';
import { Card, Button, Container, Col, Row, Spinner } from 'react-bootstrap';
import styled from '@emotion/styled';
import { GET_ALL_SPACE } from '../operations/queries/GetAllSpace';
import * as GetAllSpaceTypes from '../operations/queries/__generated__/GetAllSpace';
import { useAddMockupScene } from '../operations/mutations/AddMockupScene';
import { fetchAllSpaceVar } from '../cache';
import { useQuery } from "@apollo/client";
const { Dialog, Window } = require('@progress/kendo-react-dialogs');

// 1. add window
// 2. create mockup scene

const Home = () => {

  const limit = 5;
  const { mutate } = useAddMockupScene();
  
  const [showCreateWindow, setShowCreateWindow] = useState<Boolean>(false);
  const [windowHeight, setWindowHeight] = useState<Number>(0.0);
  const [createSceneLoading, setCreateSceneLoading] = useState<Boolean>(false);
  const [currentSelectSpace, setCurrentSelectSpace] = useState<GetAllSpaceTypes.GetAllSpace_getAllSpace>();

  // const clickElement = React.createRef<HTMLButtonElement>();

  const {
    data,
    loading,
    error,
    fetchMore
  } = useQuery<
    GetAllSpaceTypes.GetAllSpace,
    GetAllSpaceTypes.GetAllSpaceVariables
  >(GET_ALL_SPACE, { variables: { startSpaceId: '', limit } });

  if (error && !data) return <p>ERROR</p>;

  const spaces = data?.getAllSpace;
  if (spaces && fetchAllSpaceVar()) {
    fetchMore({
      variables: {
        startSpaceId: spaces[spaces.length - 1]!.id,
        limit
      }
    }).then((result): void => {
      // 已經抓取所有space 資料
      if (result.data.getAllSpace.length == 0) {
        fetchAllSpaceVar(false);
      }
    });
  }

  async function addMockupScene(space: GetAllSpaceTypes.GetAllSpace_getAllSpace | undefined) {
    setCreateSceneLoading(true)
    await mutate({ variables: { spaceId: space!.id } }).catch((err) => console.log(err))
    setCreateSceneLoading(false)
    toggleDialog(space)
  }

  function getCard(space: GetAllSpaceTypes.GetAllSpace_getAllSpace) {

    const Element = styled.div`
      padding: 10px;
    `;

    const ButtonList = styled.div`
      display: flex;
      flex-direction: row;
      justify-content:space-between;
    `;

    return (
      <Element>
        <Card style={{ width: '20rem' }}>
          <Card.Img variant="top" src={space.thumbnail!} />
          <Card.Body>
            <Card.Title>{space.name!}</Card.Title>
            <Card.Text>
              {space.description || ''}
            </Card.Text>
            <ButtonList>
              <Button variant="primary" onClick={() => window.open(`https://3dtour.istaging.com/${space.id}`, "_blank") }>前往樣版</Button>
              <Button variant="primary" onClick={() => toggleDialog(space) }>創建3D專案</Button>
            </ButtonList>
          </Card.Body>
        </Card>
      </Element>
    )
  }
  const toggleDialog = (space: GetAllSpaceTypes.GetAllSpace_getAllSpace | undefined) => {
    setShowCreateWindow(!showCreateWindow);
    setWindowHeight(window.pageYOffset + 150);
    if (space) {
      setCurrentSelectSpace(space);
    }
  }

  return (
    <div className="album py-5 bg-light">
      { showCreateWindow &&
        <Window title={"Scene"} onClose={() => toggleDialog(currentSelectSpace)} initialWidth={500} initialHeight={350} 
        initialTop={ windowHeight }>
          <form className="k-form">
            <fieldset>
              <legend>3D專案</legend>
              <label className="k-form-field">
                <span>專案名稱</span>
                <input className="k-textbox" defaultValue={currentSelectSpace?.name || ''} />
              </label>
              <label className="k-form-field">
                <span>專案描述</span>
                <input className="k-textbox" placeholder={currentSelectSpace?.description || ''} />
              </label>
            </fieldset>

            <div className="text-right">
              { createSceneLoading ? <Spinner animation="border" variant="secondary" /> : null}
              <span style={{display: 'inline-block', marginLeft: '50px'}}></span>
              <button type="button" className="k-button" onClick={() => toggleDialog(currentSelectSpace)}>Cancel</button>
              <span style={{display: 'inline-block', marginLeft: '10px'}}></span>
              <button type="button" className="k-button k-primary" onClick={() => { addMockupScene(currentSelectSpace) }}>Submit</button>
            </div>
          </form>
        </Window>}
      <Container>
        {loading ? <Spinner animation="border" variant="secondary" /> : null}
        <Row>
          {spaces?.map(
            (space) =>
              <Col key={space!.id}>
                {getCard(space!)}
              </Col>
          )
          }
        </Row>
      </Container>
    </div>
  )
};

export default Home;