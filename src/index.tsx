import React from 'react';
import ReactDOM from 'react-dom';
import '@progress/kendo-theme-bootstrap/dist/all.css';
import './index.css';
import App from './containers/App';
import reportWebVitals from './reportWebVitals';
import { ApolloClient, ApolloProvider } from '@apollo/client';
import './css/bootstrap.scss';
import { cache } from './cache';

const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjYmM4ZjIyMDJmNjZkMWIxZTEwMTY1OTFhZTIxNTZiZTM5NWM2ZDciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdnJtYWtlci1wcm8tZGV2IiwiYXVkIjoidnJtYWtlci1wcm8tZGV2IiwiYXV0aF90aW1lIjoxNjA4NTU3NDM3LCJ1c2VyX2lkIjoid2Z6VkdlbzhUQU9icXlNNDVERGMySEpEekw5MiIsInN1YiI6IndmelZHZW84VEFPYnF5TTQ1RERjMkhKRHpMOTIiLCJpYXQiOjE2MDg1NTc0MzcsImV4cCI6MTYwODU2MTAzNywiZW1haWwiOiJmZWxpeEBpc3RhZ2luZy5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiZmVsaXhAaXN0YWdpbmcuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.kRGDMs6QWb68TZ-eooMv15CWn1LQ0YEE71NVwAK17BX6brQlC6K-75UecdxMQRStH0TiA8vgLheXzJJDJLtzE7RHzem85aB1WAvLnbv-V1oa4a6iHypWf57ReQO_QGbhhVAQ_UR_KUDu62e6Jl7-ggn3lwdjYv11Rrz752_WJT5W4AnrGFN1xnbGTeV_jhUhyQHuVkUx0FjYpsnJPPzB7m-mHPuEIuv0RR6CKXK3UmqcXghuRzrDdVMkNP9FZR0eltTUhNl8xUuYR4dZqAADWjvWKIlN8HCi9a_jnZAP5udxhs2BDkBydhTNXfN0Vvz2pxYqH0B5gYglfdEAxSGRSA'
// token expire every 3600 seconds
export const client = new ApolloClient({
  cache,
  uri: 'http://localhost:4000/graphql',
  headers: {
    authorization: `Bearer ${token}`,
    'client-name': 'react-typescript-3d',
    'client-version': '1.0.0',
  },
  connectToDevTools: true,
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
)

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
