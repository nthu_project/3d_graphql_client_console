/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum MockupMediaType {
  GIF = "GIF",
  GLB = "GLB",
  ICON = "ICON",
  IMAGE = "IMAGE",
  TEXT = "TEXT",
  VIDEO = "VIDEO",
  WEB = "WEB",
}

export enum MutationStatus {
  FAIL = "FAIL",
  SUCCESS = "SUCCESS",
}

export interface InputPosition {
  x?: number | null;
  y?: number | null;
  z?: number | null;
}

export interface InputRotation {
  x?: number | null;
  y?: number | null;
  z?: number | null;
  w?: number | null;
}

export interface InputScale {
  x?: number | null;
  y?: number | null;
  z?: number | null;
}

export interface MockupTagInput {
  name: string;
  id?: string | null;
  fileUrl?: string | null;
  type: MockupMediaType;
  maxWidth?: number | null;
  isVertical?: boolean | null;
  followCamera?: boolean | null;
  openLink?: string | null;
  thumb?: string | null;
  extData?: any | null;
  position: InputPosition;
  rotation: InputRotation;
  scale: InputScale;
}

export interface UpdateMockupTagInput {
  add?: (MockupTagInput | null)[] | null;
  del?: (string | null)[] | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
