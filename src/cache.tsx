
import { InMemoryCache, ReactiveVar, makeVar } from "@apollo/client";

export const cache: InMemoryCache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        getAllSpace: {
          keyArgs: false,
          merge(existing = [], incoming) {
            return [...existing, ...incoming];
          },
        }
      }
    }
  }
});

/**
 * Set initial values when we create cache variables.
 */
export const currentSceneIdVar: ReactiveVar<string> = makeVar<string>('');
export const fetchAllSpaceVar: ReactiveVar<boolean> = makeVar<boolean>(true);
