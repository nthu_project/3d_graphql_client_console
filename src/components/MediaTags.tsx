import React from 'react'
import PropTypes from 'prop-types'
import { ListGroup } from 'react-bootstrap';
import styled from '@emotion/styled';

interface TagProps {
  label: String;
  children?: any;
}

const MediaTags: React.FC<TagProps> = ({ label }) => {
  const Tags = styled.div`
    position: block;
    margin-left: auto;
    margin-right: auto;
    padding-top: 30px;
    text-align: center;
  `;
  const textStyle = {
    color: "blue"
  };
  return (
    <Tags>
      <p className="font-weight-bold" style={textStyle}>{label}</p>
      <ListGroup>
        <ListGroup.Item>Cras justo odio</ListGroup.Item>
        <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
        <ListGroup.Item>Morbi leo risus</ListGroup.Item>
        <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
        <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
      </ListGroup>
    </Tags>
  )
}

export default MediaTags;