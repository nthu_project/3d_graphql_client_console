/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetAllSpace
// ====================================================

export interface GetAllSpace_getAllSpace {
  __typename: "Space";
  id: string;
  name: string | null;
  description: string | null;
  thumbnail: string | null;
}

export interface GetAllSpace {
  getAllSpace: (GetAllSpace_getAllSpace | null)[];
}

export interface GetAllSpaceVariables {
  startSpaceId?: string | null;
  limit?: number | null;
}
