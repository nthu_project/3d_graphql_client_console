/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TagData
// ====================================================

export interface TagData_position {
  __typename: "Position";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface TagData_rotation {
  __typename: "Rotation";
  x: number | null;
  y: number | null;
  z: number | null;
  w: number | null;
}

export interface TagData_scale {
  __typename: "Scale";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface TagData {
  __typename: "MockupTag";
  id: string;
  position: TagData_position;
  rotation: TagData_rotation;
  scale: TagData_scale;
  data: any | null;
}
