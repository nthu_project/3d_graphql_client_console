/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetMockupScene
// ====================================================

export interface GetMockupScene_mockupScene_tags_position {
  __typename: "Position";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface GetMockupScene_mockupScene_tags_rotation {
  __typename: "Rotation";
  x: number | null;
  y: number | null;
  z: number | null;
  w: number | null;
}

export interface GetMockupScene_mockupScene_tags_scale {
  __typename: "Scale";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface GetMockupScene_mockupScene_tags {
  __typename: "MockupTag";
  id: string;
  position: GetMockupScene_mockupScene_tags_position;
  rotation: GetMockupScene_mockupScene_tags_rotation;
  scale: GetMockupScene_mockupScene_tags_scale;
  data: any | null;
}

export interface GetMockupScene_mockupScene_space_hotSpots_position {
  __typename: "Position";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface GetMockupScene_mockupScene_space_hotSpots_rotation {
  __typename: "Rotation";
  x: number | null;
  y: number | null;
  z: number | null;
  w: number | null;
}

export interface GetMockupScene_mockupScene_space_hotSpots {
  __typename: "HotSpot";
  id: string;
  floor: number | null;
  position: GetMockupScene_mockupScene_space_hotSpots_position | null;
  rotation: GetMockupScene_mockupScene_space_hotSpots_rotation | null;
  cameraHeight: number | null;
  higherSkybox: (string | null)[] | null;
  lowerSkybox: (string | null)[] | null;
}

export interface GetMockupScene_mockupScene_space {
  __typename: "Space";
  id: string;
  isMp: boolean | null;
  glbUrl: string | null;
  hotSpots: (GetMockupScene_mockupScene_space_hotSpots | null)[] | null;
}

export interface GetMockupScene_mockupScene {
  __typename: "MockupScene";
  id: string;
  name: string | null;
  creatorUid: string;
  thumbnail: string | null;
  description: string | null;
  tags: (GetMockupScene_mockupScene_tags | null)[] | null;
  space: GetMockupScene_mockupScene_space | null;
}

export interface GetMockupScene {
  mockupScene: GetMockupScene_mockupScene | null;
}

export interface GetMockupSceneVariables {
  sceneId: string;
}
