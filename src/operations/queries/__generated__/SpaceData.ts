/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SpaceData
// ====================================================

export interface SpaceData_hotSpots_position {
  __typename: "Position";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface SpaceData_hotSpots_rotation {
  __typename: "Rotation";
  x: number | null;
  y: number | null;
  z: number | null;
  w: number | null;
}

export interface SpaceData_hotSpots {
  __typename: "HotSpot";
  id: string;
  floor: number | null;
  position: SpaceData_hotSpots_position | null;
  rotation: SpaceData_hotSpots_rotation | null;
  cameraHeight: number | null;
  higherSkybox: (string | null)[] | null;
  lowerSkybox: (string | null)[] | null;
}

export interface SpaceData {
  __typename: "Space";
  id: string;
  isMp: boolean | null;
  glbUrl: string | null;
  hotSpots: (SpaceData_hotSpots | null)[] | null;
}
