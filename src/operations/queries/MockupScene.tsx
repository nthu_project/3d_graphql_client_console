import { gql } from "@apollo/client";

export const SPACE_DATA = gql`
  fragment SpaceData on Space {
    __typename
    id
    isMp
    glbUrl
    hotSpots {
      id
      floor
      position {
        x
        y
        z
      }
      rotation {
        x
        y
        z
        w
      }
      cameraHeight
      higherSkybox
      lowerSkybox
    }
  }
`;

export const TAG_DATA = gql`
  fragment TagData on MockupTag {
    id
    position {
      x
      y
      z
    }
    rotation {
      x
      y
      z
      w
    }
    scale {
      x
      y
      z
    }
    data
  }
`

export const GET_SCENE_BY_ID = gql`
  query GetMockupScene($sceneId: ID!) {
    mockupScene(id: $sceneId) {
      id
      name
      creatorUid
      thumbnail
      description
      tags {
        ...TagData
      }
      space {
        ...SpaceData
      }
    }
  }
  ${SPACE_DATA}
  ${TAG_DATA}
`