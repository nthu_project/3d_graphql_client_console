import { gql } from "@apollo/client";

export const GET_ALL_SPACE = gql`
  query GetAllSpace($startSpaceId: String, $limit: Int) {
    getAllSpace(startSpaceId: $startSpaceId, limit: $limit) {
      id
      name
      description
      thumbnail
    }
  }
`