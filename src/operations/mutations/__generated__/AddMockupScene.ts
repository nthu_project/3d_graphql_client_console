/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MutationStatus } from "./../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: AddMockupScene
// ====================================================

export interface AddMockupScene_addMockupScene_scene {
  __typename: "MockupScene";
  id: string;
  name: string | null;
  updatedAt: string;
}

export interface AddMockupScene_addMockupScene {
  __typename: "MockupScenePayload";
  status: MutationStatus;
  message: string | null;
  scene: AddMockupScene_addMockupScene_scene | null;
}

export interface AddMockupScene {
  addMockupScene: AddMockupScene_addMockupScene | null;
}

export interface AddMockupSceneVariables {
  spaceId: string;
}
