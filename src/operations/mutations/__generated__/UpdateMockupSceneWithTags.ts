/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateMockupTagInput, MutationStatus } from "./../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateMockupSceneWithTags
// ====================================================

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_position {
  __typename: "Position";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_rotation {
  __typename: "Rotation";
  x: number | null;
  y: number | null;
  z: number | null;
  w: number | null;
}

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_scale {
  __typename: "Scale";
  x: number | null;
  y: number | null;
  z: number | null;
}

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags {
  __typename: "MockupTag";
  id: string;
  position: UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_position;
  rotation: UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_rotation;
  scale: UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags_scale;
  data: any | null;
}

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene {
  __typename: "MockupScene";
  id: string;
  name: string | null;
  tags: (UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene_tags | null)[] | null;
}

export interface UpdateMockupSceneWithTags_updateMockupSceneWithTags {
  __typename: "MockupScenePayload";
  status: MutationStatus;
  message: string | null;
  scene: UpdateMockupSceneWithTags_updateMockupSceneWithTags_scene | null;
}

export interface UpdateMockupSceneWithTags {
  updateMockupSceneWithTags: UpdateMockupSceneWithTags_updateMockupSceneWithTags | null;
}

export interface UpdateMockupSceneWithTagsVariables {
  id: string;
  input: UpdateMockupTagInput;
}
