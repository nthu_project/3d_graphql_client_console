import { gql, useMutation } from "@apollo/client";
import * as AddMockupSceneTypes from './__generated__/AddMockupScene';
import { currentSceneIdVar } from '../../cache';

export const ADD_MOCKUPSCENE = gql`
  mutation AddMockupScene($spaceId: ID!) {
    addMockupScene(spaceId: $spaceId) {
      status
      message
      scene {
        id 
        name
        updatedAt
      }
    }
  }
`

export function useAddMockupScene() {
  const [mutate, { data, error }] = useMutation<
    AddMockupSceneTypes.AddMockupScene, 
    AddMockupSceneTypes.AddMockupSceneVariables
  >(
    ADD_MOCKUPSCENE,
    {
      onCompleted(data: AddMockupSceneTypes.AddMockupScene): void {
        // error: Error while verifying Firebase ID token
        if (data.addMockupScene?.status == "FAIL") {
          // console.log(data.addMockupScene);
          throw new Error(`useAddMockupScene fail, and message: ${data.addMockupScene?.message}`)
        }
        // 目前場景ID到Cache
        currentSceneIdVar(data.addMockupScene?.scene?.id);
      }
    }
  )

  return { mutate, data, error };
}