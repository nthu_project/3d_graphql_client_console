import { gql, useMutation } from "@apollo/client";
import * as UpdateMockupSceneTypes from './__generated__/UpdateMockupSceneWithTags';
import { GetMockupScene, GetMockupSceneVariables } from '../queries/__generated__/GetMockupScene';
import { GET_SCENE_BY_ID } from '../queries/MockupScene';

export const UPDATE_MOCKUPSCENE = gql`
  mutation UpdateMockupSceneWithTags($id: ID!, $input: UpdateMockupTagInput!) {
    updateMockupSceneWithTags(id: $id, input: $input) {
      status
      message
      scene {
        id
        name
        tags {
          id
          position { x  y  z}
          rotation { x  y  z  w}
          scale {x  y  z}
          data
        }
      }
    }
  }
`

export function useUpdateMockupScene() {
  const [mutate, { data, error }] = useMutation<
    UpdateMockupSceneTypes.UpdateMockupSceneWithTags, 
    UpdateMockupSceneTypes.UpdateMockupSceneWithTagsVariables
  >(
    UPDATE_MOCKUPSCENE,
    {
      update(cache, {data}): void {
        const mockupSceneResponse = data?.updateMockupSceneWithTags?.scene;
        const existingMockupScene = cache.readQuery<GetMockupScene, GetMockupSceneVariables>({
          query: GET_SCENE_BY_ID,
          variables: {
            sceneId: mockupSceneResponse?.id || ''
          }
        });
        // replace current cache on mockup scene tags 
        if (existingMockupScene && mockupSceneResponse) {
          cache.writeQuery({
            query: GET_SCENE_BY_ID,
            data: {
              tags: mockupSceneResponse.tags,
            }
          });
        }
      }    
    }
  )
  
  return { mutate, data, error };
}